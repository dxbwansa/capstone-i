#initate pyspark session
from pyspark import SparkContext, SparkConf
from pyspark.sql import SparkSession

#this ML:K-means calculating
from pyspark.ml.clustering import KMeans, KMeansModel
from pyspark.ml.evaluation import ClusteringEvaluator

# these are for formating data
from pyspark.sql.types import StructField, StringType, IntegerType, StructType, FloatType
from pyspark.ml.linalg import Vectors 
from pyspark.ml.feature import VectorAssembler,StandardScaler
from pyspark.sql.functions import *
from pyspark.ml import Pipeline

#this is for formating strings to numerica values
from pyspark.ml.feature import StringIndexer
import pyspark.sql.functions as F
import pymongo
import csv
import pandas as pd

#global variables
#initialize the sparksession
spark = SparkSession \
        .builder\
        .appName("Pyspark-ML") \
        .getOrCreate()
# let pyspark infer own schema for grapgh
dataset = spark.read.csv("dev.csv",header=True,inferSchema=True)

#main functions run everything
def main():
    column_name = filtering()
    df = convert(column_name)
    final_data = transform(df)   
    predictions = KmeansCalc(final_data)
    normalization_function()


#filters through data for columns with string Type
def filtering():
    type_list = []
    categoricalColumns = []
    type_list = dataset.dtypes
    for i in type_list:
        if i[1] == 'string':
            categoricalColumns.append(i[0])
    return categoricalColumns

#converting strings to numeric values
def convert(categoricalColumns):
    #define a list of stages in your pipeline. The string indexer will be one stage
    stages = []
    dataset = spark.read.csv("dev.csv",header=True,inferSchema=True)
    
    #iterate through all categorical values
    for categoricalCol in categoricalColumns:
        #create a string indexer for those categorical values and assign a new name including the word 'Index'
        stringIndexer = StringIndexer(inputCol = categoricalCol, outputCol = categoricalCol + 'Index')
    
        #append the string Indexer to our list of stages
        stages += [stringIndexer]

    #Create the pipeline. Assign the satges list to the pipeline key word stages
    pipeline = Pipeline(stages = stages)
    #fit the pipeline to our dataframe
    pipelineModel = pipeline.fit(dataset)
    #transform the dataframe
    dataset = pipelineModel.transform(dataset)

    #drop string column and rename column for index
    for categoricalCol in categoricalColumns:
        dataset = dataset.drop(categoricalCol)
        dataset = dataset.withColumnRenamed((categoricalCol + 'Index'), categoricalCol)

    return dataset

#transform data
def transform(df):
    
    vec_assembler = VectorAssembler(inputCols =list(df.columns), outputCol='features')
    final_data = vec_assembler.setHandleInvalid("skip").transform(df)
    
    scaler = StandardScaler(inputCol="features", outputCol="scaledFeatures", withStd=True, withMean=False)
    
    # Compute summary statistics by fitting the StandardScaler
    scalerModel = scaler.fit(final_data)
    # Normalize each feature to have unit standard deviation.
    final_data = scalerModel.transform(final_data)
    
    return final_data

#Training the Model and Evaluate
#Training a k-means model
def KmeansCalc(final_data):
    kmeans = KMeans(featuresCol='scaledFeatures').setK(2).setSeed(1)
    model = kmeans.fit(final_data)

    # Evaluate clustering by computing Silhouette score
    evaluator = ClusteringEvaluator()
    # Make predictions
    predictions = model.transform(final_data)
    # Evaluate clustering by computing Silhouette score
    silhouette = evaluator.evaluate(predictions)
    print("Silhouette with squared euclidean distance = " + str(silhouette))

    # Evaluate clustering
    cost = model.summary.trainingCost
    print("Within Set Sum of Squared Errors = " + str(cost))
    
    # Shows the result
    print("Cluster Centers: ")
    ctr = []
    centers = model.clusterCenters()
    for center in centers:
        ctr.append(center)
        print(center)

    # create a dataframe containing the centers and their coordinates
    d_clusters = {int(i):[float(centers[i][j]) for j in range(len(centers[i]))] 
                  for i in range(len(centers))}

    # Let's create a dataframe containing the centers and their coordinates
    df_centers = spark.sparkContext.parallelize([(k,)+(v,) for k,v in 
    d_clusters.items()]).toDF(['prediction','center'])

    predictions = predictions.withColumn('prediction',F.col('prediction')\
                                         .cast(IntegerType()))
    predictions = predictions.join(df_centers,on='prediction',how='left')
    
    #calculating the distance
    get_dist = F.udf(lambda features, center : 
                 float(features.squared_distance(center)),FloatType())
    predictions = predictions.withColumn('dist',\
                                         get_dist(F.col('features'),F.col('center')))

    #writes out the selected columns to a csv
    predictions.select("features","prediction","dist").toPandas().to_csv('results.csv')


#nomalizes the data for distance  
def normalization_function():   
    with open('results.csv', 'r') as file:
        reader = csv.reader(file)
        
        #saves and skips header
        header =  next(reader, None)
        max = 0
        buffer = []
        dist = len(line)-1
        
        #get max distance
        for line in reader:
            buffer.append(line)
            if float(line[dist]) > max:
                max = float(line[dist])
        #normalize the distance    
        for line in buffer:
            line[dist] = (float(line[dist]) / max)
        
    with open ('OUTPUT.csv', 'w', newline="") as file_out:
        writer = csv.writer(file_out)
        #adds back in the header
        writer.writerow(header)
        writer.writerows(buffer)


        
# Run main() if script called directly
if __name__ == "__main__":
        main()

# Create the client
client = pymongo.MongoClient('localhost', 27017)

# Connect to our database
db = client['CapstoneDB']

# Fetch our series collection
series_collection = db['devData']

# Function to parse csv to dictionary
data = []
with open("output.csv", "r")  as csvfile:
    reader = csv.reader(csvfile)
    header = next(reader)
    for row in reader:
        doc={}
        for n in range(0,len(header)):
            doc[header[n]] = row[n]
        #insert data into database
        db.devData.insert_one(doc)


