##https://www.bmc.com/blogs/python-spark-k-means-example/

#importing libraries for use
import pandas as pd
from pyspark.sql import SparkSession
from pyspark.sql.types import *

#this ML:K-means calculating
from pyspark.ml.clustering import KMeans
from pyspark.ml.evaluation import ClusteringEvaluator

# these are for formting data
from pyspark.ml.linalg import Vectors 
from pyspark.ml.feature import VectorAssembler,StandardScaler
from pyspark.sql.functions import *

#this is for formating strings to numerica values
from pyspark.ml.feature import StringIndexer
import pyspark.sql.functions as F

#initialize the sparksession
spark = SparkSession \
    .builder\
    .appName("pySpark Example") \
    .getOrCreate()

# let pyspark infer own schema for grapgh
dataset = spark.read.csv("dev.csv",header=True,inferSchema=True)
dataset.head()
#dataset.describe().show()

#prints out what the columns of dataset
dataset.columns

#converting strings to numeric values (for the dev.csv specifically)
# 1. create new column
# 2. delete original column
# 3. rename new column

#protocol type
protocol_indexer = StringIndexer(inputCol="protocol_type",
                                      outputCol="protocol_typeIndex")
protocol_stringIdx_model = protocol_indexer.fit(dataset)
dataset = protocol_stringIdx_model.transform(dataset)
dataset = dataset.drop("protocol_type")


#service 
service_indexer = StringIndexer(inputCol="service",
                                      outputCol="serviceIndex")
service_stringIdx_model = service_indexer.fit(dataset)
dataset = service_stringIdx_model.transform(dataset)
dataset = dataset.drop("service")


#flag 
flag_indexer = StringIndexer(inputCol="flag",
                                      outputCol="flagIndex")
flag_stringIdx_model = flag_indexer.fit(dataset)
dataset = flag_stringIdx_model.transform(dataset)
dataset = dataset.drop("flag")

##dataset.show()



#transform data
vec_assembler = VectorAssembler(inputCols =list(dataset.columns), outputCol='features')
final_data = vec_assembler.setHandleInvalid("skip").transform(dataset)#.select('id', 'features')

#Test data
##final_data.show()

scaler = StandardScaler(inputCol="features", outputCol="scaledFeatures", withStd=True, withMean=False)


# Compute summary statistics by fitting the StandardScaler
scalerModel = scaler.fit(final_data)
# Normalize each feature to have unit standard deviation.
final_data = scalerModel.transform(final_data)


##Training the Model and Evaluate
#Training a k-means model
kmeans = KMeans(featuresCol='scaledFeatures',k=3)
model = kmeans.fit(final_data)

# Evaluate clustering by computing Silhouette score
evaluator = ClusteringEvaluator()
# Make predictions
predictions = model.transform(final_data)
# Evaluate clustering by computing Silhouette score
silhouette = evaluator.evaluate(predictions)
print("Silhouette with squared euclidean distance = " + str(silhouette))


# Evaluate clustering
cost = model.summary.trainingCost
print("Within Set Sum of Squared Errors = " + str(cost))

# Shows the result
print("Cluster Centers: ")
ctr=[]
centers = model.clusterCenters()
for center in centers:
    ctr.append(center)
    print(center)

