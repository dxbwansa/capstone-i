//Need help to refine this more

const express = require('express');
const router = require('express').Router();
let dnsT = require('../Models/dataT');  // path to model

// Save the new traffic data
const saveData = (dataT, res) => {
    note
      .save()
      .then(dataT => res.json(dataT))
      .catch(() => res.status(400).json({msg: "Error: Could not save the note."}));
  }

// Add new traffic data
router.post("/", (req, res) => {
    //Data want to add (parameters)
    const { tDate, host, IP_Add, protocolType, tURL } = req.body;
    let dataT = new DataTraffic({
        tDate
        host
        IP_Add
        protocolType
        tURL
    });
    saveData = (dataT, res);
  });
  
// Find traffic data
router.get("/", (req, res) => {
    Note.find({ }) //Not sure what to find by, could be multiple element
      .then(dataT => {
        res.json(dataT);
      }).catch(err => res.json({msg: "Could Not find notes for that user."}));
  });
  
// delete a traffic data
router.delete("/:id", (req, res) => {
    DataTraffic
      .findByIdAndDelete(req.params.id)
      .then(() => res.json({msg: "Message Deleted."}))
      .catch(() => res.json({msg: "Could not find note to delete."}));
  });

  module.exports = router;