const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Store DNS Traffic
const  DNSSchema = new Schema({
    tDate : date

    //host data
    host : string
   
    //port 53 web traffic  
    // - layer 3
    IP_Add = array //[Source, Destination]
    //source_Add : string
    //dest_Add : string
    
    // - layer 4
    protocolType : string
   
    //url
    tURL : string
})

module.exports = DataTraffic = mongoose.model('DataTraffic', DNSSchema);
