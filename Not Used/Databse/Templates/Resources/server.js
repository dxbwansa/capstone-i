//MongooseDB
const mongoose = require('mongoose');

//Express server 
const express = require('express');
const app = express();

const API_PORT = process.env.PORT || 8000;

//RESPONSE + POST
const auth = require('./middleware/auth');

app.use(express.json());

// Paste in path from MongoDB
const dbPath = ""; 

mongoose.connect(dbPath, {
    //Database Element

  }).then(() => {
    console.log("Connected to the DB.");
  }).catch((err) => console.log("Error connecting to the database."));
  
  // REPONSE + POST
  app.all('/api/*', auth);

  app.use('/api/auth', require('./routes/auth'));

  app.listen(API_PORT, () => console.log(`Listening on Port ${API_PORT}`));
