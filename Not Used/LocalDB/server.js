const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

// Connection URL -> As a specifiied user
const url = 'mongodb://Admin:$[password]@$[hostlist]/Capestone_DB?authSource=admin';

// Use connect method to connect to the Server
MongoClient.connect(url, function(err, client) {
  assert.equal(null, err);
  client.close();
});
