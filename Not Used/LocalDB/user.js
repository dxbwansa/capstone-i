//Tell us which database to use -> Creat database outside dbs
use admin

//Get connected into Monog with Admin when security is up
//mongo --port 27017  --authenticationDatabase "admin" -u "myUserAdmin" -p "myAdminTeam@1994"
//mongo --port 27017  --authenticationDatabase "admin" -u "Admin" -p "myAdminTeam@1994"
//mongo --port 27017  --authenticationDatabase "CapstoneDB" -u "myUser"
//mongo -u CapstoneDB --authenticationDatabase Admin -p "myAdminTeam@1994"

//db.auth("myAdmin", "myAdminTeam@1994")

//mongodb://myAdmin:myAdminTeam%401994@localhost:27017

//Administrator
db.createUser(
  {
    user: "Admin",
    pwd: "myAdminTeam@1994", // passwordPrompt(), // or cleartext password (myAdminTeam@1994)
    roles: [ { role: "userAdminAnyDatabase", db: "admin" }, "readWriteAnyDatabase" ]
  }
)

//db.dropUser(username)

////Don't use any dbs log in after getting in
// mongo --port 27017
// db.auth("myTestUser", "myUserTeam$1994")
//User to make edits/read data
db.createUser(
  {
    user: "myTestUser",
    pwd:  "myUserTeam$1994",
    roles: [ { role: "readWrite", db: "CapstoneDB" } ]
  }
)

db.createUser(
  {
    user: "myUserAdmin",
    pwd: passwordPrompt(), // or cleartext password myAdminTeam@1994
    roles: [ { role: "userAdminAnyDatabase", db: "admin" }, "readWriteAnyDatabase" ]
  }
)

use test
db.createUser(
  {
    user: "myTester",
    pwd: "xyz123",
    roles: [ { role: "readWrite", db: "test" },
             { role: "read", db: "reporting" } ]
  }
)

db.createUser(
  {
      user: "myAdmin",
      pwd: "myAdminTeam@1994",
      roles: [ "root" ]
  }
)
