//Command Line code -> insert csv file to database
//mongoimport --type csv -d test -c products --headerline --drop filename

//mongoexport --db [database name] --collection [collection name] --out [path of output ex. /data/dump/music/artists.json]

//Insert one record to the database
const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

// Connection URL
const url = 'mongodb://localhost:27017';

//Input document (one record) into MongoDB
MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("capstoneDB.capstone");
    var myobj = {}; // data
    dbo.collection("capstone").insertOne(myobj, function(err, res) {
      if (err) throw err;
      console.log("1 document inserted");
      db.close();
    });
  });
