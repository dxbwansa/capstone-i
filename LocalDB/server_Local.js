const MongoClient = require('mongodb').MongoClient;
const express = require("express");
const assert = require('assert');

// Setup express app
const app = express();

app.use(
  bodyParser.urlencoded({
      extended: false
  })
);

app.use(bodyParser.json());

// Connection URL
const url = 'mongodb://localhost:27017';

// Database Name
const dbName = 'CapstoneDB';

// Create a new MongoClient
const client = new MongoClient(url);

// Use connect method to connect to the Server
client.connect(function(err) {
  assert.equal(null, err);
  console.log("Connected successfully to server");

  const db = client.db(dbName);

  client.close();
});
