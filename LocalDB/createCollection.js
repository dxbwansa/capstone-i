//Create the collection

var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";

MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  var dbo = db.db("capstoneDB");

  dbo.createCollection("test", function(err, res) {
    if (err) throw err;
    console.log("Test Collection created!");
    db.close();
  });

  dbo.createCollection("fraudEmail", function(err, res) {
    if (err) throw err;
    console.log("Fraud Email Collection created!");
    db.close();
  });

  dbo.createCollection("malURL", function(err, res) {
    if (err) throw err;
    console.log("Malicious Email Collection created!");
    db.close();
  });

  dbo.createCollection("networkTraffic	", function(err, res) {
    if (err) throw err;
    console.log("Network Email Collection created!");
    db.close();
  });

  dbo.createCollection("spamEmail", function(err, res) {
    if (err) throw err;
    console.log("Spam Email Collection created!");
    db.close();
  });
});
