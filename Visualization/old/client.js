
// Import necessary modules.
import io from 'socket.io-client';
import React from 'react';
import ReactDOM from 'react-dom';
import { useEffect, useState } from 'react';
import {
  BarChart,
  Bar,
  Line,
  LineChart,
  XAxis,
  YAxis,
  Tooltip
} from 'recharts';

// Ensure MongoDB is used.
const { MongoClient } = require("mongodb");

const socket = io('[MONGODB URI HERE]', {
  transports: ['websocket', 'polling']
});
// Connection URI
const uri =
  "[MONGODB URI HERE]";
// Make a MongoDB client.
const client = new MongoClient(uri);
  try {
    // Connect the client to the server
    await client.connect();
    // Establish and verify connection
    await client.db("admin").command({ ping: 1 });
    console.log("Connected successfully to server");
  }


async function run() {
	const App = ({}) => {
	  const [data, setData] = useState([]);

	  // Wait for MongoDB to change and update the state
	  useEffect(() => {
		socket.on('networkTraffic', percentMalicious => {
		  setData(currentData => [...currentData, percentMalicious]);
		});
	  }, []);

	  // Draw the line chart using the information from MongoDB.
	  return (
		<div>
		  <h1>Malicious Traffic</h1>
		  <LineChart width={500} height={300} data={data}>
			<XAxis dataKey="name" />
			<YAxis />
			<Line dataKey="value" />
		  </LineChart>
		</div>
	  );
	  finally {
		// Ensures that the client will close when finished or on error.
		await client.close();
	  };
	};
};
	
ReactDOM.render(<App />, document.getElementById('root'));
