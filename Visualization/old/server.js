const server = require('http').createServer();
const mongodb = require('MongoDB');

const io = require('socket.io')(server, {
  transports: ['websocket', 'polling']
});

let tick = 0;
// Listen for socket connections
io.on('connection', client => {
  setInterval(() => {
    // Every second, emit event to user.
    client.update(maliciousPercent => {
      client.emit('traffic', {
        name: tick++,
        value: maliciousPercent
      });
    });
  }, 1000);
});

server.listen(3000);