/*
 * Ike-5232021: Initial Version.
 * Ike-5252021: Relaxed constraints, removal of websocket, updating format and additional imports (chartjs-plugin-streaming)
 * Ike-5272021: Alpha version, set up for integration with MongoDB, just needs a connection socket and it wll be good to go.
 * TODO: Add MongoDB connection socket and test for edge cases.
*
*/
// Import packages for use
import React, { Component } from 'react';
import { Bubble, Line, Chart } from 'react-chartjs-2';
import 'chartjs-adapter-luxon';
import StreamingPlugin from 'chartjs-plugin-streaming';
import './App.css';

class App extends Component {

  // Mount the chart and stream its contents.
  componentWillMount() {
	
	// Register the StreamingPlugin with the Chart object.
	// This allows the Chart to update with real-time information samples
	// from MongoDB.
    Chart.register(StreamingPlugin);

  }
	// Render the Chart onto the user's web browser "view"-tifully.
    render() {

    return (
		
	  // Type of Graphing.
      <Bubble

        data={{
		  // Dataset personalization.
          datasets: [{
			// Web Server information (left, red)
            label: 'Web Server',
            backgroundColor: 'rgba(255, 99, 132, 0.5)',
            borderColor: 'rgb(255, 99, 132)',
            data: []

          }, {
			// Application Server information (right, blue)
            label: 'App Server',
            backgroundColor: 'rgba(54, 162, 235, 0.5)',
            borderColor: 'rgb(54, 162, 235)',
            data: []

          }]
        }}
		
		// Setting the options for our axes.
        options={{
          scales: {
			// y-axis (vertical) options.
            y: {
              title: {

                display: true,
                text: 'Probability of Anomaly'
              }
            },

			// x-axis (horizontal) options.
            x: {
			  // Set the type to update in time with the viewer's 
              type: 'realtime',
              realtime: {
				// Delay the start of the packets so the viewer is not
				// "shocked" with information, make it "view"-tiful!
                delay: 1000,
				
				// When the chart is updated, grab these values.
                onRefresh: chart => {
					
				  // Iterate through the charts and grab these values.
                  chart.data.datasets.forEach(dataset => {

                    dataset.data.push({
					  // Get the time of the packet.
                      x: fileGet(data.csv):[0],
					  // Get the k-value of the packet
					  // DISTANCE FROM LEARNED CLUSTER
                      y: fileGet(data.csv):[9],
					  // We haven't implemented this feature yet, so it stays random for now.
                      r: Math.random() * 100
                    });
                  });
},
			  // Display the title for the horizontal axis.
              title: {

              display: true,
              text: 'Packet Timestamp'
              }
            }
          }
        }}
      />
    );
  }
}

export default App;
