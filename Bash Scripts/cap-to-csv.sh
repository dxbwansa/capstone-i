#!/bin/bash
#
# Script for capturing all packets from network adapter ens192 into a ring
# buffer of .pcap files.  These files are then parsed for the following
# information, which is then sent to a different server in the form of .csv
# files.
# •  Frame timestamp
# •  Source and destination IP addresses
# •  If the traffic is TCP or UDP
# •  Source and destination ports
#
# For this script to work properly the following pre-requisites must be met:
# •  Tshark package installed.
# •  Linux utilities lsof and scp are available.
# •  Directory where .pcap and .csv files are written (variable temp_dir)
#        should have well over 256MB of space and not be used for any other
#        purpose.
# •  The pub ssh key for root on the server with this script needs to be
#        copied over to the system account’s (variable usr_dest) home/.ssh
#        folder on the destination server (variable server_dest).
#        See https://developers.redhat.com/blog/2018/11/02/how-to-manually-copy-ssh-keys-rhel
# •  The user usr_dest or one of it’s groups needs “rwx” file
#        permissions to the destination folder (variable server_dest).
# •  This script must be run as root (this is checked for) for lsof to work
#        properly.
#
# 20200523 joshg: Initial version
# 20200526 joshg: Moved initial sleep inside of for loop to fix bug with
#                 occasional empty .csv files
#
# Initial variables
temp_dir="/mnt/ramdisk"
usr_dest="pcap-to-mern"
server_dest="192.168.49.218"
dir_dest="/csv-dump"
cap_time=20
file_qty=100

# Testing variables.  Should be commnedted out for production
# cap_time=5
# file_qty=10
# dir_dest="/test"


# For lsof to work properly, it needs to be run as root.  This is the only
# part of the script that needs root access
if [ "$EUID" -ne 0 ]
    then
        echo -e "\n************************"
        echo "   Please run as root"
        echo -e "************************\n"
    exit
fi
rm $temp_dir/*.log -f
rm $temp_dir/*.csv -f
rm $temp_dir/*.pcap -f

# While unlikely to be rached in 20 seconds, the filesize ring buffer parameter
# is used becasue the RAMdisk where the .pcap and .csv files are temporarily
# stored is only 1GB, so to make sure the .pcap files don’t get too big and
# fill up the tmpfs (which was chosen over the more size dynamic but slower ramfs)
tshark -B 100 -Q -b duration:$cap_time -b filesize:256000 -a files:$file_qty -i ens192 \
        -w $temp_dir/capture.pcap 2>>$temp_dir/capture-error.log &

for num in `seq 1 $file_qty`; do
    text_num=`printf %05d $num`
    pcap_file_name="$temp_dir/capture_${text_num}"
    csv_file_name="$temp_dir/capture_${num}"

# Tshark takes between 0.2 and 0.3 seconds to open and begin writing to the
# first file.  Without this sleep, the script check to see if a file is open
# will fail to work as tshark hasn’t created the first capture file, so the
# script will race ahead of the .pcap file currently being written to, causing
# all the created .csv files to be blank.  This can also occur in the middle 
# of the ring buffer if the .pcap to .csv conversion is especially quick do
# a small quantity of packets being captured.
    sleep_cntr=0.5
    sleep 0.5

# Checks to see if the .pcap file targeted by the variable num is done being
# written to, and if not pauses for half of a second.
    while [[ `lsof -c dumpcap | grep $pcap_file_name` ]]; do
        sleep 0.5
        sleep_cntr=$(bc<<<"0.5 + $sleep_cntr")
    done

    tshark -r $pcap_file_name*.pcap -n -T fields \
            -e frame.time_epoch -e ip.src -e ip.dst -e ip.proto \
            -e tcp.srcport -e tcp.dstport -e udp.srcport -e udp.dstport \
            -E quote=d -E occurrence=a -E header=y -E separator=, \
            1> $csv_file_name.csv 2> $temp_dir/capture-error.log

    scp -q $csv_file_name.csv $usr_dest@$server_dest:$dir_dest/
    rm $pcap_file_name*.pcap
    rm $csv_file_name.csv
    printf "File %04s of %04s done at `date +"%r"` with %s seconds of sleeping \
            for the file\n" $num $file_qty $sleep_cntr \
            >> $temp_dir/capture-error.log

done
